/*
* @description: Utility class for holding Invocable functions for Process Builder
* @author: mha
* @lastmodify:
*/
public class ProcessBuilderInvocables {
    
  @InvocableMethod(label='Close Timecards' description='Closes all Timecards before the Timesheet creation')
  public static void closeTimeCards(List<Datetime> timesheetCreatedDate) {
    List<Time_Card__c> tcList = new List<Time_Card__c>();
    tcList = [SELECT Id, Is_Closed__c FROM Time_Card__c WHERE Is_Closed__c = false AND CreatedDate < :timesheetCreatedDate[0]];
    
    for (Time_Card__c tc : tcList) {
      tc.Is_Closed__c = true;
    }
      if(!tcList.isEmpty()) 
      { 
        try{ 
      		update tcList;
        }catch (Exception e)
        {
            //Log Message
        }
      }
  }
}