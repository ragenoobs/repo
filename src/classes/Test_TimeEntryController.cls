/*
* @description: Controller extension for visualforce page TimeEntry
* @author: mha
* @lastmodify:
*/
@isTest(SeeAlldata=false)
public class Test_TimeEntryController {
    public static testmethod void testTEController(){
       Test_Utils.TestDataWrapper tdw = Test_Utils.initTestData();
        
       System.currentPageReference().getParameters().put('id', tdw.tc.id); 
        
       Test.startTest();
           TimeEntryController ctr = new TimeEntryController(new ApexPages.StandardController(tdw.tc));
          // ctr.tcID = tdw.tc.id;
           ctr.initTimeCard();
           ctr.toggleStartStopClock();
           
           //System.assertNotEquals(0, ctr.entryList.size());
           //System.assertEquals(true, ctr.isTimerStarted);
            
           ctr.toggleStartStopClock();   
           ctr.toggleStartStopClock();
        
           ctr.te.Started_At__c = Datetime.now().addHours(-5); 
           ctr.te.Hours__c = 0;       	
           ctr.selectedTCid = ctr.te.Id;
        	
           ctr.trackTime();
       Test.stopTest();
           
       System.assertEquals(true, ctr.isTimerStarted);        
       System.assertNotEquals(0, ctr.entryList.size());
        
    }
   public static testmethod void testTEControllerFromProject(){
       Test_Utils.TestDataWrapper tdw = Test_Utils.initTestData();
        
       System.currentPageReference().getParameters().put('projid', tdw.proj.id); 
       TimeEntryController ctr = new TimeEntryController(new ApexPages.StandardController(tdw.tc));
	   
       Test.startTest();
       	ctr.initTimeCard();    
       Test.stopTest(); 
    }
    public static testmethod void testTEControllerFromTask(){
       Test_Utils.TestDataWrapper tdw = Test_Utils.initTestData();
        
       System.currentPageReference().getParameters().put('taskid', tdw.ca.id); 
       TimeEntryController ctr = new TimeEntryController(new ApexPages.StandardController(tdw.tc));
	   
       Test.startTest();
       	ctr.initTimeCard();    
       Test.stopTest(); 
    }
}