/**
*	Schedulable Batch Class for Timesheet Generation once a week on Sunday
*
*	@author mha
*/

global class TimesheetGenerateSchedBatch implements Database.batchable<sObject>, Schedulable 
{
	global void schedule()
	{
		String schedName = 'Timesheet Generate Sched Batch';
		String cronSettings = '0 0 19 ? * SUN';
			if(!Test.isRunningTest())
			{
				system.schedule(schedName, cronSettings, new TimesheetGenerateSchedBatch());
			}
	}
	global void execute(SchedulableContext sc)
	{
		Database.executebatch(this);
	}

	global Database.QueryLocator start(Database.BatchableContext bc) 
	{
		String query = 'SELECT Id,Total_Cost__c, Hours_Total__c, Cost_per_Hour__c FROM Time_Card__c Where Is_Closed__c = false';
		//system.debug('query:'+query);
        if(Test.isRunningTest()){
            query += ' Limit 1';
        }
		return Database.getQueryLocator(query);
	}

	global void execute (Database.BatchableContext BC, List<sObject> scope)
	{
		//try
		//{
			performExecute(scope);
		/*}
		catch(Exception e)
		{
			//Log error into a file
		}*/
	}
	
	private void performExecute (List<sObject> scope)
	{
        List<Time_Card__c> tcList = scope;
        Decimal hours = 0;
        Decimal cost = 0;        
        for(Time_Card__c tc : tcList){
            hours += tc.Hours_Total__c;
            cost += tc.Total_Cost__c;
        }
        //If Data becomes too large it might require the Batch to run few times
        Timesheet__c tsheet;
        List<Timesheet__c> tsheetList = [Select Id, Total_Hours__c, Total_Income__c, Num_of_Time_Cards__c  
                              From Timesheet__c
                              Where CreatedDate = today 
                              Limit 1];
        if(tsheetList == null || tsheetList.size() == 0){
            tsheet = new Timesheet__c(Frequency__c = 'Weekly', Total_Hours__c = 0, 
                                      Total_Income__c = 0, Num_of_Time_Cards__c = 0);
        }else{
            tsheet = tsheetList[0];
        }
        
        tsheet.Total_Hours__c += hours;
        tsheet.Total_Income__c += cost;
        tsheet.Num_of_Time_Cards__c += tcList.size();
        
        try{
            upsert tsheet;
        }catch (Exception e){
            //Log
        }       
	}
	
	global void finish(Database.BatchableContext BC)
	{
        //Send success/error email
	}
}