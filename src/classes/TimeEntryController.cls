/*
* @description: Controller extension for visualforce page TimeEntry
* @author: mha
* @lastmodify:
*/
public with sharing class TimeEntryController {
    public List<Time_Entry__c> entryList {
        get {
        	return [SELECT Id, Name, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, isActive__c,
                    Started_At__c, Stopped_At__c, Time_Card__c, Hours__c 
                    FROM Time_Entry__c
                    Where Time_Card__c = :ApexPages.currentPage().getParameters().get('id')
                    AND (CreatedDate = today OR isActive__c = true)
                    Order By isActive__c DESC,LastModifiedDate DESC
                    Limit 20];
        }
        set;
    }
    public boolean isTimerStarted{
        get{
            if(entryList != null && entryList.size()>0 && entryList[0].isActive__c){
                isTimerStarted = true;
                te = entryList[0];
                return true;
            }else{
                return false;
            } 
        }
     	private set;
    }
    public String errorMessage{get;set;}    
    public Id selectedTCid {get;set;}
    
    private static final Id userId = Userinfo.getUserId();
    private Id projID;
    private Id taskID;
    private Id tcID; 
    @TestVisible
    private Time_Entry__c te;
    
    public TimeEntryController (ApexPages.StandardController ctrl){
        errorMessage = null; 
        tcID = ApexPages.currentPage().getParameters().get('id');
        taskID = ApexPages.currentPage().getParameters().get('taskid');
        projID = ApexPages.currentPage().getParameters().get('projid');
    }
    
    /*
     *  @description: Starts/Stop timer and records hours for Rollup summary
     */ 
    public PageReference toggleStartStopClock(){
        if(te == null){
            te = new Time_Entry__c(isActive__c = true, Time_Card__c = tcID, Started_At__c = Datetime.now(), Hours__c = 0);            
        }else if(te.isActive__c){
           te.isActive__c = false; 
           te.Stopped_At__c = Datetime.now(); 
           te.Hours__c += getHoursDifference(te.Started_At__c, te.Stopped_At__c); 
        }else{
            te = new Time_Entry__c(isActive__c = true, Time_Card__c = tcID);           
        }
        
        try{
            upsert te;
        }catch (Exception e){
                errorMessage = e.getMessage();
        }
        return null;
    }
    /*
     * @description: Stops the current active Time Entry and Starts the selected one from the page 
     */
    public PageReference trackTime(){
        if(te != null && te.isActive__c){
            toggleStartStopClock();
        }
        system.debug(Logginglevel.ERROR, 'selectedTCid:'+selectedTCid); 
        te = [SELECT Id, Name, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, isActive__c,
                    Started_At__c, Stopped_At__c, Time_Card__c, Hours__c 
                    FROM Time_Entry__c
                    Where Id = :selectedTCid];
        
       te.Started_At__c = Datetime.now(); 
       te.isActive__c = true;
        
       try{
            update te;
        }catch (Exception e){
                errorMessage = e.getMessage();
        }
        return null;
    }
    /* 
     * @description: Returns the difference of 2 Datetime fields in hours
	 */	
    private static Decimal getHoursDifference (Datetime firstDate, Datetime lastDate){
        return Math.Floor(
            (lastDate.getTime()  - firstDate.getTime())/ (1000.0*60.0*60.0)                     
           			);
    }
    
    /*
     * @description: Used to create the correct data model - time card and parents if not available 
     * Page can be called with Project, Task or Timecard ids
     * Task id check for latest timecard for user and if not available creates one
     * Project checks for Time card and if not available creates task + time card
	*/
    public PageReference initTimeCard(){        
        if(String.isBlank(tcID)){
            if(String.isNotBlank(taskID)){
                //Create Time Entry From Task                
                Time_Card__c tc;
                List<Time_Card__c> tcList = [Select Id From Time_Card__c 
                      Where Task_ID__c = :taskID AND User_ID__c = :Userinfo.getUserId()
                      AND is_Closed__c = false
                      Order By LastModifiedDate Desc
                      Limit 1];
                if(tcList == null || tcList.size() < 1){
                	tc = new Time_Card__c(Task_ID__c = taskID, User_ID__c = userId);
                	insert tc;
                }else{
                    tc = tcList[0];
                }
                
                tcID = tc.Id;
                Apexpages.Currentpage().getParameters().put('id',tcID);
            }else if(String.isNotBlank(projid)){
                //Create Time Entry From Project
                Time_Card__c tc;
                List<Time_Card__c> tcList = [Select Id From Time_Card__c 
                      Where Project_Id__c = :projid AND User_ID__c = :userId
                      AND is_Closed__c = false
                      Limit 1];
                if(tcList == null || tcList.size() < 1){
                    Case c;
                    List<Case> cList = [Select Id 
                         From Case 
                         Where Project__c = :projid 
                         Order By LastModifiedDate Desc
                      	 Limit 1];
                    if(cList == null || cList.size() < 1){
                    	c = new Case(Origin = 'Web', Project__c = projid, Subject = 'AutoGenerated');                   
                        insert c;
                    }else{
                        c = cList[0]; 
                    }
                    
                	tc = new Time_Card__c(Task_ID__c = c.ID, User_ID__c = userId);
                	insert tc;
                }else{
                    tc = tcList[0];
                }
                tcID = tc.Id;
                Apexpages.Currentpage().getParameters().put('id',tcID);
            }else{
                errorMessage = 'Unexpected Error';
            }            
            PageReference pr = Apexpages.Currentpage();
            pr.setRedirect(true);
            return pr;
        }
        return null;
    }
}