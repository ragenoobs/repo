/*
* @description: Utils class for test init data
* @author: mha
* @lastmodify:
*/
public class Test_Utils {
    
    public static TestDataWrapper initTestData(){
        TestDataWrapper tdw = new TestDataWrapper();
        
        tdw.acc = createAccount('TestAccount');
        tdw.proj = createProject(tdw.acc.Id);
        tdw.ca = createCase(tdw.proj.Id);
        tdw.tc = createTimeCard(tdw.ca.Id);
        
        return tdw;
    }
    
    public static Account createAccount (String name){
        Account a = new Account (Name = name);
        insert a;
        return a;
    }    
    
    public static Project__c createProject (Id accountID){
        Project__c p = new Project__c (Account__c = accountID, Rate__c = 0);
        insert p;
        return p;
    }    
    public static Case createCase (Id projectId){
        Case c = new Case (Origin = 'Web', Project__c = projectId, Rate__c = 0);
        insert c;
        return c;
    }   
    
    public static Time_Card__c createTimeCard (Id taskId){
        Time_Card__c tc = new Time_Card__c (Task_Id__c = taskId, User_Id__c = Userinfo.getUserId(),
                                           is_Closed__c = false);
        insert tc;
        return tc;
    }
    
    public class TestDataWrapper{
        public Account acc;
        public Project__c proj;
        public Case ca;
        public Time_Card__c tc;
    }
}