/*
* Trigger handler HNDL_TimeEntry for Time_Entry__c
* @author mha
* @lastmodify
*/
public class HNDL_TimeEntry extends TriggerManager.TriggerHandler {

	// Handler variables         
    private static final String IS_CLOSED_MESSAGE = 'The Time Card is already closed.';
	
	// Constructor
    public HNDL_TimeEntry() {
    	 super('HNDL_TimeEntry', Time_Entry__c.sObjectType);
    }
     
    //public override void beforeInsert(SObject so) {}
    
    //public override void beforeUpdate(SObject oldSo, SObject so) {}
    
    public override void bulkBefore(List <SObject> soLst, Map<Id,sObject> oldMap, Map<Id,sObject> newMap){	
        if (Trigger.isInsert){
    		bulkBeforeInsert(soLst, oldMap, newMap);
    	}else if (Trigger.isUpdate){
    		bulkBeforeUpdate(soLst, oldMap, newMap);
    	}else if (Trigger.isDelete){
            bulkBeforeDelete(soLst, oldMap, newMap);
        }
    }
    
     public override void bulkAfter(List <SObject> soLst, Map<Id,sObject> oldMap, Map<Id,sObject> newMap) {
     	 
    	if (Trigger.isInsert){
    		bulkAfterInsert(soLst, oldMap, newMap);
    	}else if (Trigger.isUpdate){
    		bulkAfterUpdate(soLst, oldMap, newMap);
    	}
         
     }
     
    
    //public override void afterInsert(SObject so) {}    
    
    //public override void afterUpdate(SObject oldSo, SObject so) {}
    
    //public override void beforeDelete(SObject so) {}
    
    //public override void afterDelete(SObject so) {}
    
    public static void bulkBeforeInsert(List <SObject> soLst, Map<Id,sObject> oldMap, Map<Id,sObject> newMap){
    	List <Time_Entry__c> teList = soLst;
        checkIfParentIsClosed(teList);
    }
 
    public static void bulkBeforeUpdate(List <SObject> soLst, Map<Id,sObject> oldMap, Map<Id,sObject> newMap){
        List <Time_Entry__c> teList = soLst;        
        checkIfParentIsClosed(teList);
    }
    
    public static void bulkBeforeDelete(List <SObject> soLst, Map<Id,sObject> oldMap, Map<Id,sObject> newMap){
    
    }  
    
    public static void bulkAfterInsert(List <SObject> soLst, Map<Id,sObject> oldMap, Map<Id,sObject> newMap){
    
    }
    
    public static void bulkAfterUpdate(List <SObject> soLst, Map<Id,sObject> oldMap, Map<Id,sObject> newMap){
        	
    }
    
    // Static void method checkIfParentIsClosed used to add an Error to the record
    // if the parent is in status Closed
    private static void checkIfParentIsClosed (List <Time_Entry__c> teList){
        Set<Id> tcIDs = new Set<ID>();
        for (Time_Entry__c te : teList){
            tcIDs.add(te.Time_Card__c);
        }
        
        Map<Id,Time_Card__c> idTCMap = new Map<Id,Time_Card__c>(
            [Select Id,is_Closed__c From Time_Card__c Where id IN :tcIDs]);
        
        Time_Card__c tempTC;
        for (Time_Entry__c te : teList){
            tempTC = idTCMap.get(te.Time_Card__c);
            if(tempTC.is_Closed__c){
                te.addError(IS_CLOSED_MESSAGE);
            }
        }
    }
    
}