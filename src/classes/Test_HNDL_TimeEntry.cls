/*
* @description: Test class trigger handler TimeEntry
* @author: mha
* @lastmodify:
*/
@isTest(SeeAlldata=false)
public class Test_HNDL_TimeEntry {
   public static testmethod void testParentIsClosed(){
       Test_Utils.TestDataWrapper tdw = Test_Utils.initTestData();
       tdw.tc.is_Closed__c = true;
       update tdw.tc;
       
       Time_Entry__c te = new Time_Entry__c(Time_Card__c = tdw.tc.id);
       try{
           insert te;
       }catch (Exception e){
           System.assertEquals(true, String.isNotBlank(e.getMessage()));
       }       
    }
}