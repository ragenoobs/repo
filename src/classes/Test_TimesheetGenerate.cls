/*
* @description: Test class for Timesheet Generate Schedulable Batch
* @author: mha
* @lastmodify:
*/
@isTest(SeeAlldata=false)
public class Test_TimesheetGenerate {
  public static testmethod void testTimesheetBatch(){
        
       Test_Utils.TestDataWrapper tdw = Test_Utils.initTestData(); 
      
       Test.startTest();
           TimesheetGenerateSchedBatch bat = new TimesheetGenerateSchedBatch();
           bat.schedule();
           Database.executeBatch(bat);
       Test.stopTest();
      
       List<Timesheet__c> tsheetList = [Select Id
                              From Timesheet__c
                              Where CreatedDate = today 
                              Limit 1]; 
       
      System.assertEquals(1, tsheetList.size());
    }
}