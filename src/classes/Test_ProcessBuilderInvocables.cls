/*
* @description: Test class ProcessBuilderInvocables
* @author: mha
* @lastmodify:
*/
@isTest(SeeAlldata=false)
public class Test_ProcessBuilderInvocables {
     public static testmethod void testParentIsClosed(){
       Test_Utils.TestDataWrapper tdw = Test_Utils.initTestData();
         
       Test.startTest();
       	ProcessBuilderInvocables.closeTimeCards(new List<Datetime>{Datetime.now().addHours(1)});      
        tdw.tc = [Select is_closed__c From Time_Card__c Where Id = :tdw.tc.Id];
       Test.stopTest();
         
       system.assertEquals(true, tdw.tc.is_closed__c);
     }

}