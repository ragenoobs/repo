trigger trTimeEntry on Time_Entry__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	TriggerManager.createHandler(HNDL_TimeEntry.class);
}