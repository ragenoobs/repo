/*
* Trigger handler HNDL_TimeCard for Time_Card__c
* @author mha
* @lastmodify
*/

public with sharing class HNDL_TimeCard extends TriggerManager.TriggerHandler {

	// Handler variables    
    private static final String IS_CLOSED_MESSAGE = 'The Time Card is already closed.';
    private static final String FIELDSET_NAME = 'isclosederrorfields';
	
	// Constructor
    public HNDL_TimeCard() {
    	 super('HNDL_TimeCard', Time_Card__c.sObjectType);
    }
     
    //public override void beforeInsert(SObject so) {}
    
    //public override void beforeUpdate(SObject oldSo, SObject so) {}
    
    public override void bulkBefore(List <SObject> soLst, Map<Id,sObject> oldMap, Map<Id,sObject> newMap){	
        if (Trigger.isInsert){
    		bulkBeforeInsert(soLst, oldMap, newMap);
    	}else if (Trigger.isUpdate){
    		bulkBeforeUpdate(soLst, oldMap, newMap);
    	}else if (Trigger.isDelete){
            bulkBeforeDelete(soLst, oldMap, newMap);
        }
    }
    
     public override void bulkAfter(List <SObject> soLst, Map<Id,sObject> oldMap, Map<Id,sObject> newMap) {     	 
    	if (Trigger.isInsert){
    		bulkAfterInsert(soLst, oldMap, newMap);
    	}else if (Trigger.isUpdate){
    		bulkAfterUpdate(soLst, oldMap, newMap);
    	}
         
     }
     
    
    //public override void afterInsert(SObject so) {}    
    
    //public override void afterUpdate(SObject oldSo, SObject so) {}
    
    //public override void beforeDelete(SObject so) {}
    
    //public override void afterDelete(SObject so) {}
    
    public static void bulkBeforeInsert(List <SObject> soLst, Map<Id,sObject> oldMap, Map<Id,sObject> newMap){
    	List<Time_Card__c> tcLst = soLst;
        populateRate(tcLst);
    }
 
    public static void bulkBeforeUpdate(List <SObject> soLst, Map<Id,sObject> oldMap, Map<Id,sObject> newMap){
    	List<Time_Card__c> tcLst = soLst;
        Map<Id,Time_Card__c> oldTcMap = (Map<Id,Time_Card__c>) oldMap;        
        List<Time_Card__c> closedTcLst = new List<Time_Card__c>();
        
        for(Time_Card__c tc : tcLst){
            if(tc.is_Closed__c){
                closedTcLst.add(tc);
            }
        }
        
        if(!closedTcLst.isEmpty()){
            checkFieldSetFieldsForChange(closedTcLst,oldTcMap);
        }
    }
    
    public static void bulkBeforeDelete(List <SObject> soLst, Map<Id,sObject> oldMap, Map<Id,sObject> newMap){
    
    }  
    
    public static void bulkAfterInsert(List <SObject> soLst, Map<Id,sObject> oldMap, Map<Id,sObject> newMap){
    
    }
    
    public static void bulkAfterUpdate(List <SObject> soLst, Map<Id,sObject> oldMap, Map<Id,sObject> newMap){
        	
    }
    /*
     * @description: Initializes Rate
	*/
    private static void populateRate(List<Time_Card__c> tcLst){
        //If Rate__c is not Required field - error handling
        Decimal userRate = [Select Rate__c From User Where Id = :UserInfo.getUserId()][0].Rate__c;
        
        Set<Id> taskIds = new Set<Id>();
        for(Time_Card__c tc : tcLst){
            taskIds.add(tc.Task_ID__c);
        }
        Map<Id,Case> idTaskMap = new Map<Id,Case>([Select Id, Rate__c, Project__r.Rate__c 
                                                   From Case
                                                  	Where Id IN :taskIds]);
        Case tempTask;
        for(Time_Card__c tc : tcLst){
            tempTask = idTaskMap.get(tc.Task_ID__c);
			tc.Cost_per_Hour__c = chooseRate(userRate, tempTask.Project__r.Rate__c, tempTask.Rate__c);   
        }
        
    }
    
    /* 
     * @description: Used to select the most relevant rate
	*/
    private static Decimal chooseRate(Decimal userRate, Decimal projRate, Decimal taskRate){
        if(taskRate > 0 && taskRate > userRate){
            return taskRate;
        }else if (projRate > 0 && projRate > userRate){
            return projRate;
        }else{
           return userRate;
        }
    }
    
    /* 
     * @description: checks if fields from a field set have been changed and adds an error
	*/
    private static void checkFieldSetFieldsForChange (List <Time_Card__c> tcLst, Map<Id,Time_Card__c> oldMap){
        Set<String> fieldSet = new Set<String>();
        //dynamically get the fields from the field set and then use the same for comparison in the trigger. 
        for(Schema.FieldSetMember fields : Schema.SObjectType.Time_Card__c.fieldSets.getMap().get(FIELDSET_NAME).getFields()){
            fieldSet.add(fields.getFieldPath());
        }
        for(Time_Card__c tc : tcLst){
            for(string s: fieldSet){
                if(tc.get(s) != oldMap.get(tc.Id).get(s)){
                    tc.addError(IS_CLOSED_MESSAGE);
                    break;
                }
            }
        }
    }
}