trigger trTimeCard on Time_Card__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	TriggerManager.createHandler(HNDL_TimeCard.class);
}