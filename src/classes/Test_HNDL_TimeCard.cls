/*
* @description: Test class trigger handler TimeCard
* @author: mha
* @lastmodify:
*/
@isTest(SeeAlldata=false)
public class Test_HNDL_TimeCard {
    public static testmethod void testInsert(){
        
       Test_Utils.TestDataWrapper tdw = Test_Utils.initTestData();   
       Decimal userRate = [Select Rate__c from User Where Id = :Userinfo.getUserId()][0].Rate__c;  
       tdw.tc = [Select Cost_per_Hour__c From Time_Card__c Where Id = :tdw.tc.Id];
       System.assertEquals(userRate, tdw.tc.Cost_per_Hour__c);         
        
    }
   public static testmethod void testUpdate(){
       Test_Utils.TestDataWrapper tdw = Test_Utils.initTestData();
       tdw.tc.is_Closed__c = true;
       update tdw.tc;
       
       tdw.tc.Cost_per_Hour__c = 30;
       try{
           update tdw.tc;
       }catch (Exception e){
           System.assertEquals(true, String.isNotBlank(e.getMessage()));
       }
		
		delete tdw.tc;      
    }
}